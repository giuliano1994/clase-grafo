package grafo;

import java.util.HashSet;
import java.util.Set;


public class Grafo 
{
	// representamos al grafo a travez de su matriz de adyacencia
	private boolean [][] matriz;
	
	// el tama�o viene pre fijado a travez del constructor
	public Grafo(int vertices) 
	{
		matriz = new boolean [vertices][vertices]; // matriz con todos en falso
		
	}
	
	
	
	public void agregarArista (int fila, int columna) 
	{
	
		verificarFila(fila);
		
		verificarColumna(columna);
		
		filaIgualAColumna(fila, columna);
		
		matriz[fila][columna] = true;
		matriz[columna][fila] = true;
		
	}

	
	public boolean existeArista (int fila, int columna) 
	{
		
		verificarFila(fila);
		
		verificarColumna(columna);
		
		return matriz[fila][columna];
		
	}
	
	
	public void eliminarArista (int fila, int columna) 
	{
	
		verificarFila(fila);
		
		verificarColumna(columna);
		
		filaIgualAColumna(fila, columna);
		
		matriz[fila][columna] = false;
		matriz[columna][fila] = false;
		
	}

	
	public int tama�o ()
	{
		
		return matriz.length;
		
	}
	
	public Set<Integer> devolverVecinos(int vertice)
	{
		
		verificarFila(vertice);
		
		Set <Integer> retorno = new HashSet<Integer>();
		
		for (int i = 0; i<tama�o(); i++) 
		{			
			if (existeArista(vertice, i)) 
			{	
				retorno.add(i);	
			}
		}
		return retorno;
	}
	
	


	
	
	
	
	
	private void filaIgualAColumna(int fila, int columna) {
		if (columna == fila)
		{
			throw new IllegalArgumentException("la columna " + columna + "y la fila " + fila + " no pueden ser iguales");
		}
	}


	private void verificarColumna(int columna) {
		if (columna <0)
		{
			throw new IllegalArgumentException("la columna " + columna + " eleguida no puede ser menos a 0");
		}
			
		if (columna > matriz.length)
		{
			throw new IllegalArgumentException("la columna " + columna + " eleguida no puede ser mayor a la matriz");
		}
	}


	private void verificarFila(int fila) {
		if (fila <0)
		{
			throw new IllegalArgumentException("la fila " + fila + " eleguida no puede ser menos a 0");
		}
			
		if (fila >= matriz.length)
		{
			throw new IllegalArgumentException("la fila " + fila + " eleguida no puede ser mayor a la matriz");
		}
	}

	
}
