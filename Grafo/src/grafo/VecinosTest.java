package grafo;

import static org.junit.Assert.*;

import org.junit.Test;

public class VecinosTest {

	
	
	@Test (expected = IllegalArgumentException.class)
	public void indiceNegativoTest() 
	{
	
		Grafo grafo = new Grafo(5);
		grafo.devolverVecinos(-1);
		
	}

	
	@Test (expected = IllegalArgumentException.class)
	public void indiceExedidoTest() 
	{
	
		Grafo grafo = new Grafo(5);
		grafo.devolverVecinos(5);
		
	}

	
	@Test
	public void todosAisladosTest() 
	{
	
		Grafo grafo = new Grafo(5);
		
		assertEquals(0 , grafo.devolverVecinos(2).size());
		
	}

	
	@Test
	public void vecinoDeTodosTest()
	{
		
		Grafo grafo = new Grafo (4);
		
		grafo.agregarArista(1, 0);
		grafo.agregarArista(1, 2);
		grafo.agregarArista(1, 3);
		
		int [] esperado = {0, 2, 3};
		
		AssertParaTest.iguales(esperado, grafo.devolverVecinos(1));

	}
	
	


	@Test
	public void verticeNormalTest() 
	{
	
		Grafo grafo = new Grafo(5);
		
		grafo.agregarArista(1, 3);
		grafo.agregarArista(2, 3);
		grafo.agregarArista(2, 4);
		
		int [] iguales = {1, 2};
		
		AssertParaTest.iguales(iguales, grafo.devolverVecinos(3));
		
	}
	
	
	
	
	
	
	
	
}
