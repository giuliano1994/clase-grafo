package grafo;

import static org.junit.Assert.*;

import org.junit.Test;

public class EdisionDeAristasTest {

	
	@Test
	public void aristaExistenteTest() 
	{
	
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3);
		assertTrue(grafo.existeArista(2, 3));
		
	}
	
	
	@Test
	public void aristaOpuestaTest() 
	{
	
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3);
		assertTrue(grafo.existeArista(3, 2));
		
	}

	
	@Test
	public void aristaInexistenteTest() 
	{
	
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3);
		assertFalse(grafo.existeArista(3, 3));
		
	}
	
	
	@Test (expected = IllegalArgumentException.class)
	public void primerVerticegrafoNegativoTest() 
	{
		
		Grafo grafo = new Grafo (5);
		grafo.agregarArista(-3, 3);
		
	}

	
	@Test (expected = IllegalArgumentException.class)
	public void primerVerticegrafoExedidoTest() 
	{
		
		Grafo grafo = new Grafo (5);
		grafo.agregarArista(6, 3);
		
	}
	
	
	@Test (expected = IllegalArgumentException.class)
	public void segundoVerticegrafoNegativoTest() 
	{
		
		Grafo grafo = new Grafo (5);
		grafo.agregarArista(-3, 3);
		
	}

	
	@Test (expected = IllegalArgumentException.class)
	public void segundoVerticegrafoExedidoTest() 
	{
		
		Grafo grafo = new Grafo (5);
		grafo.agregarArista(5, 3);
		
	}
	
	
	@Test (expected = IllegalArgumentException.class)
	public void VerticesIgualesgrafoExedidoTest() 
	{
		
		Grafo grafo = new Grafo (5);
		grafo.agregarArista(3, 3);
		
	}
	 
	
	@Test
	public void eliminarAristaTest() 
	{
	
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3);
		grafo.eliminarArista(2, 3);
		assertFalse(grafo.existeArista(2, 3));
		
	}
	
	
	@Test
	public void agregararistaDosVecesTest() 
	{
	
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3);
		grafo.agregarArista(2, 3);
		assertTrue(grafo.existeArista(2, 3));
		
	}
	
	
	@Test
	public void eliminarAristaDosVecesTest() 
	{
	
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(2, 3);
		grafo.eliminarArista(2, 3);
		grafo.eliminarArista(2, 3);
		assertFalse(grafo.existeArista(2, 3));
		
	}

	
	
	
	
}
